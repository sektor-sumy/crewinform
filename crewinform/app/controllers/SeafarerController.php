<?php

class SeafarerController extends BaseController {

		public function index()
	  {
	    $seafarer = Seafarer::all();
	    /*$seafarer =DB::table('crew_seafarer')
		            ->leftjoin('crew_country', 'crew_seafarer.Addrcountry', '=', 'crew_country.id')
		            ->leftjoin('crew_city', 'crew_seafarer.Addrcity', '=', 'crew_city.id')
		            ->get();*/
	    $title = "seafarer";
        return View::make('seafarer.index')
			->with(compact('title'))
			->with(compact('seafarer'));
	  }

	  public function getCity($id)
	    {
	        if (Request::ajax())
	        {
	            $positions = DB::table('crew_city')->select('id','name_en')->where('country_id', '=', $id)->get();
	            return Response::json( $positions );
	        } 
	    }

	  /**
	   * Show the form for creating a new resource.
	   *
	   * @return Response
	   */
	  public function create()
	  {
	  	$seafarer = Seafarer::getSelectInfo();
	  	$title = "create seafarer";
	    return View::make('seafarer.create')
	    	->with(compact(['seafarer','title']));
	  }

	  /**
	   * Store a newly created resource in storage.
	   *
	   * @return Response
	   */
	 public function store()
    {
        $file = Input::file('img');
		$destinationPath = '/image/'.Sentry::getUser()->id.'/avatar/';
		// If the uploads fail due to file system, you can try doing public_path().'/uploads' 
		$filename = str_random(12); 
		$upload_success = Input::file('img')->move('public'.$destinationPath, $filename)->resize(120, 120)->save();
		Input::merge(array('avatar'=>$destinationPath.$filename));
		$input = Input::except('img', 'ed_degree_lvl', 'dip_number', 'ed_estbl_name', 'country_estbl', 'city_estbl', 'year_grad', 'remarks');
		$validation = Validator::make($input, Seafarer::$rules);
        if ($validation->passes())
        {
        	$documents = Input::only('ed_degree_lvl', 'dip_number', 'ed_estbl_name', 'country_estbl', 'city_estbl', 'year_grad', 'remarks');
            $data = Seafarer::create($input);
            $lastInsertedId = $data->id;
            Seafarer::setHistory($documents, $lastInsertedId);
            return Redirect::route('seafarer.index');
        }

        return Redirect::route('seafarer.create')
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }

	  /**
	   * Display the specified resource.
	   *
	   * @param  int  $id
	   * @return Response
	   */
	  public function show($id)
	  {
	    $title = "Show seafarer";

	    $seaf = Seafarer::getInfo($id);
	    $comments = DB::table('crew_comments')
	  				->where('seafarer_id', $id)
	  				->join('crew_users', 'crew_comments.user_id', '=', 'crew_users.id')
            		->select('crew_comments.comment', 'crew_users.username','crew_comments.user_id', 'crew_users.id')
            		->get();
        if (is_null($seaf))
        {
            return Redirect::route('seafarer.index');
        }
        return View::make('seafarer.show')
        	->with(compact('title'))
        	->with(compact('comments'))
			->with(compact('seaf'));
	  }

	  /**
	   * Show the form for editing the specified resource.
	   *
	   * @param  int  $id
	   * @return Response
	   */
	  public function edit($id)
	  {
	  	$title = "edit seafarer";
	  	$seafarer = Seafarer::find($id);
	  	$select = Seafarer::getSelectInfo();
	  	$history = DB::table('crew_ed_history')
	  				->where('seafarer_id', $id)
	  				->first();
	  	$city = DB::table('crew_city')
	  				->whereIn('country_id', array($seafarer->Addrcountry, $seafarer->country_of_birth, $history->country_estbl))
	  				->orderBy('name_en', 'asc')
	  				->lists('name_en', 'id');
        if (is_null($seafarer))
        {
            return Redirect::route('seafarer.index');
        }
        return View::make('seafarer.edit')
        	->with(compact(['title','seafarer','city','history','select']));
	  }

	  /**
	   * Update the specified resource in storage.
	   *
	   * @param  int  $id
	   * @return Response
	   */
	  public function update($id)
		{
				$file = Input::file('img');
				if ($file) {
					$destinationPath = '/image/'.Sentry::getUser()->id.'/avatar/';
					// If the uploads fail due to file system, you can try doing public_path().'/uploads' 
					$filename = str_random(12); 
					$upload_success = Input::file('img')->move('public'.$destinationPath, $filename);
					Input::merge(array('avatar'=>$destinationPath.$filename));
				}
				$input = Input::except('img', 'ed_degree_lvl', 'dip_number', 'ed_estbl_name', 'country_estbl', 'city_estbl', 'year_grad', 'remarks');
				$validator = Validator::make(
				    array(
				        'first_name' => Input::get('first_name'),
						'last_name' => Input::get('last_name')
				    ),
				    array(
				        'first_name' => 'required',
				        'last_name' => 'required'
				    )
				);
				if ($validator->fails()) {
				  // Переданные данные не прошли проверку.
					$messages = $validator->messages();
					throw new Exception($messages->first());
				}
					$documents = Input::only('ed_degree_lvl', 'dip_number', 'ed_estbl_name', 'country_estbl', 'city_estbl', 'year_grad', 'remarks');
					$seafarer = Seafarer::find($id);
		            $seafarer->update($input);
		            $lastInsertedId = $seafarer->id;
            		Seafarer::updateHistory($documents, $lastInsertedId);
						return Redirect::to(route('seafarer.index'));
	   		
		}


		public function comment()
		{
				Input::merge(array('user_id'=>Sentry::getUser()->id));
				$input = Input::all();
				$title = 'Show seafarer';
				$validator = Validator::make(
				    array(
				        'comment' => Input::get('comment'),
				    ),
				    array(
				        'comment' => 'required'
				    )
				);
				if ($validator->fails()) {
				  // Переданные данные не прошли проверку.
					$messages = $validator->messages();
					throw new Exception($messages->first());
				}
					$data = Comment::create($input);
					return Redirect::back()->with('message','Operation Successful !');
	   		
		}

	  /**
	   * Remove the specified resource from storage.
	   *
	   * @param  int  $id
	   * @return Response
	   */
		 public function destroy($id)
	    {
	        Seafarer::find($id)->delete();
	        return Redirect::route('seafarer.index');
	    }
	  

}
