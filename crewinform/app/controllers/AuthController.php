<?php
/**
 * app/controllers/AuthController.php
 */

class AuthController extends BaseController {

	/**
     * Отображает страницу входа
     *
     * @return Illuminate\View\View
     */

	public function getLogin()
	{
		$title = 'Login';
		return View::make('auth.login', compact('title'));
	}

	/**
     * register form
     * @return Illuminate\View\View
     */
    public function getRegister()
	{
		$title = 'Register';
		return View::make('auth.register', compact('title'));
	}

	/**
     * Аутентифицирует и редиректит в админку
     *
     * @return Illuminate\Http\RedirectResponse
     */

	public function postLogin()
    {
        Input::flash();

        try {
            $credentials = array(
                'username' => Input::get('username'), 
                'password' => Input::get('password')
            );
            $user = Sentry::authenticate($credentials, Input::get('remember-me'));
        } catch (Exception $e) {
            return Redirect::to(route('auth.login'))
                ->withErrors(array($e->getMessage()));
        }
	if ($user->hasAccess('superuser'))
    {
        return Redirect::intended(route('admin'));
    }
    else
    {
        return Redirect::route('showWelcome');
    }
        
    }

    /**
     * register
     *
     * @return Illuminate\Http\RedirectResponse
     */
    public function postRegister()
	{
		try
		{			$validator = Validator::make(
			    array(
			        'first_name' => Input::get('first_name'),
					'last_name' => Input::get('last_name'),
					'email' => Input::get('email'),
					'password' => Input::get('password'),
					'username' => Input::get('username')
			    ),
			    array(
			        'first_name' => 'required',
			        'last_name' => 'required',
			        'password' => 'required|min:6',
			        'username' => 'required|unique:crew_users',
			        'email' => 'required|email|unique:crew_users'
			    )
			);
			if ($validator->fails()) {
			  // Переданные данные не прошли проверку.
				$messages = $validator->messages();
				throw new Exception($messages->first());
			}
				$user = Sentry::createUser(array(
					'first_name' => Input::get('first_name'),
					'last_name' => Input::get('last_name'),
					'email' => Input::get('email'),
					'password' => Input::get('password'),
					'username' => Input::get('username'),
					'activated' => true,
					'permissions' => array(
			            'user' => 1,
			        ),
				));

				$activationCode = $user->getActivationCode();
				return Redirect::to(route('auth.login'));
		}
		catch (Exception $e)
		{
			return Redirect::to(route('auth.register'))
                ->withErrors(array($e->getMessage()));
		}
	}

    /**
     * Обрабатывает выход
     *
     * @return Illuminate\Http\RedirectResponse
     */
    public function getLogout()
    {
        Sentry::logout();
        return Redirect::route('auth.login');
    }

}
