<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{
		$title = 'Crewinform';
		return View::make('pages.homepage', compact('title'));
	}

	public function profile()
	{
		$user = Sentry::getUser();
		$title = 'My profile';
		return View::make('profile.profile') 
			->with(compact('title'))
			->with(compact('user'));
	}

	public function edit($id)
	{
		try
		{
		    // Find the user using the user id
		    $user = Sentry::findUserById($id);
		    $title = 'Edit profile';
		    return View::make('profile.edit')
			    ->with(compact('title'))
				->with(compact('user'));
		}
		catch (Cartalyst\Sentry\Users\UserExistsException $e)
		{
		    echo 'User with this login already exists.';
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
		    echo 'User was not found.';
		}
		
	}

	public function update($id)
	{
					
			$validator = Validator::make(
			    array(
			        'first_name' => Input::get('first_name'),
					'last_name' => Input::get('last_name'),
					'email' => Input::get('email'),
					'username' => Input::get('username')
			    ),
			    array(
			        'first_name' => 'required',
			        'last_name' => 'required',
			        'username' => 'required',
			        'email' => 'required|email'
			    )
			);
			if ($validator->fails()) {
			  // Переданные данные не прошли проверку.
				$messages = $validator->messages();
				throw new Exception($messages->first());
			}
				$user = Sentry::findUserByID($id);
					$user->first_name = Input::get('first_name');
					$user->last_name  = Input::get('last_name');
					$user->email      = Input::get('email');
					$user->username   = Input::get('username');
					$user->save();
					return Redirect::to(route('profile.profile'));
   		
	}

	public function getAdmin()
    {

    	try
		{
		    $user = Sentry::getUser();
	    	if ($user->hasAccess('superuser'))
		    {
		        $users = Sentry::findAllUsers();
	        	return View::make('admin.dashboard', compact('users'));
		    }
		    else
		    {
		        return View::make('hello');
	    	}
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
		    // User wasn't found, should only happen if the user was deleted
		    // when they were already logged in or had a "remember me" cookie set
		    // and they were deleted.
		}
    	
    	
    }

    public function getUsers()
    {

    	try
		{
		    $user = Sentry::getUser();
	    	if ($user->hasAccess('superuser'))
		    {
		        $users = Sentry::findAllUsers();
	        	return View::make('admin.user.users', compact('users'));
		    }
		    else
		    {
		        return View::make('hello');
	    	}
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
		    // User wasn't found, should only happen if the user was deleted
		    // when they were already logged in or had a "remember me" cookie set
		    // and they were deleted.
		}
    	
    	
    }

}
