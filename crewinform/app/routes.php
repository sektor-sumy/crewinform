<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/




Route::get('contact', array(
        'as' => 'contact', 
        'uses' => 'HomeController@showWelcome'
));

Route::get('/seafarer/{id}/city', 'SeafarerController@getCity');

Route::group(array('before' => 'guest'), function () 
{
    Route::get('login', array(
        'as' => 'auth.login', 
        'uses' => 'AuthController@getLogin'
    ));

    Route::get('register', array(
        'as' => 'auth.register', 
        'uses' => 'AuthController@getRegister'
    ));

    Route::post('login', array(
        'before' => 'csrf',
        'uses' => 'AuthController@postLogin'
    ));

    Route::post('register', array(
        'before' => 'csrf',
        'uses' => 'AuthController@postRegister'
    ));
});

Route::group(array('before' => 'auth'), function ()
{
    Route::get('/', array(
        'as' => 'showWelcome', 
        'uses' => 'SeafarerController@index'
    ));
    Route::get('/seafarers', array(
        'as' => 'seafarer.index', 
        'uses' => 'SeafarerController@index'
    ));

    Route::post('/seafarers/store', array(
        'as' => 'seafarer.store', 
        'uses' => 'SeafarerController@store'
    ));

    Route::get('/seafarers/create', array(
        'as' => 'seafarer.create', 
        'uses' => 'SeafarerController@create'
    ));

     Route::get('/seafarers/{id}/edit', array(
        'as' => 'seafarer.edit', 
        'uses' => 'SeafarerController@edit'
    ));

     Route::get('/seafarers/{id}/show', array(
        'as' => 'seafarer.show', 
        'uses' => 'SeafarerController@show'
    ));

     Route::delete('/seafarers/{id}/destroy', array(
        'as' => 'seafarer.destroy', 
        'uses' => 'SeafarerController@destroy'
    ));

     
    Route::put('/seafarers/{id}/update', [
      'uses' => 'SeafarerController@update',
      'as' => 'seafarer.update'
    ]);

    Route::post('/seafarers/comment', array(
        'as' => 'seafarer.comment', 
        'uses' => 'SeafarerController@comment'
    ));

    Route::get('/profile', array(
        'as' => 'profile.profile', 
        'uses' => 'HomeController@profile'
    ));
    //Route::post('/profile/{id}/update',['as' => 'profile.update', 'uses' => 'HomeController@update']);
    //Route::post('/profile/{id}/update','HomeController@update');

    Route::put('/profile/{id}/update', [
      'uses' => 'HomeController@update',
      'as' => 'profile.update'
    ]);

    Route::get('/profile/{id}/edit', array(
        'as' => 'profile.edit', 
        'uses' => 'HomeController@edit'
    ));
    
    Route::get('admin', array(
        'as' => 'admin',
        'uses' => 'HomeController@getAdmin',
    ));

    Route::get('admin/view-users', array(
        'as' => 'admin/view-users',
        'uses' => 'HomeController@getUsers',
    ));

    Route::get('logout', array(
        'as' => 'auth.logout',
        'uses' => 'AuthController@getLogout'
    ));
}); 