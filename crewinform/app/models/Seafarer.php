<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;


class Seafarer extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'crew_seafarer';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */

	protected $guarded = array('id');
  	
	  public static $rules = array(
	    'last_name' => 'required|last_name'
	  );

	 public function comments()
	  {
	    return $this->hasMany('Comment');
	  }

	public static function getInfo($id)
    {
        $userInfo = DB::table('crew_seafarer')->where('crew_seafarer.id',$id)
		      ->join('crew_gender','crew_seafarer.Gender','=','crew_gender.id')
		      ->join('crew_maritaltype','crew_seafarer.maritalstatus','=','crew_maritaltype.id')
		      ->join('crew_eye_color','crew_seafarer.eyescolour','=','crew_eye_color.id')
		      ->join('crew_hair_color','crew_seafarer.haircolour','=','crew_hair_color.id')
		      ->join('crew_country','crew_seafarer.addrcountry','=','crew_country.id')
		      ->join('crew_city','crew_seafarer.addrcity','=','crew_city.id')
		      ->join('crew_ranks','crew_seafarer.rank','=','crew_ranks.id')
		      ->join('crew_ranktype','crew_ranks.rank_type','=','crew_ranktype.id')
		      ->join('crew_departments','crew_ranks.department','=','crew_departments.id')
		      ->join('crew_ed_history','crew_seafarer.id','=','crew_ed_history.seafarer_id')
		      /*->join('crew_seaservice','crew_seafarer.id','=','crew_seaservice.seafarer_id')
		      ->join('crew_vessels','crew_seaservice.vessel_id','=','crew_vessels.id')
		      ->join('crew_vesseltype_full','crew_vessels.vesseltype','=','crew_vesseltype_full.id')
		      ->join('crew_vessels_data','crew_vessels.imo','=','crew_vessels_data.imo')*/
		      ->select('*','crew_eye_color.name_en as eye_name',
		      			   'crew_hair_color.name_en as hair_name',
		      			   'crew_country.name_en as country',
		      			   'crew_city.name_en as city',
		      			   'crew_ranks.name_en as rank',
		      			   'crew_ranktype.name_en as ranktype',
		      			   'crew_departments.name_en as department',
		      			   'crew_maritaltype.name_en as marital_name', 
		      			   'crew_seafarer.id as id')
		      ->first();
		 return $userInfo;
  }

  public static function setHistory($query, $id){
  		DB::table('crew_ed_history')->insert(
    array('seafarer_id' => $id,
    	'ed_degree_lvl' => $query['ed_degree_lvl'],
    	'dip_number'    => $query['dip_number'],
    	'ed_estbl_name' => $query['ed_estbl_name'],
    	'country_estbl' => $query['country_estbl'],
    	'city_estbl'    => $query['city_estbl'],
    	'year_grad'     => $query['year_grad'],
    	'remarks'       => $query['remarks']
    	));

  }

  public static function updateHistory($query, $id){
  		DB::table('crew_ed_history')
  				->where('seafarer_id', $id)
  				->update(array(
			  		'ed_degree_lvl' => $query['ed_degree_lvl'],
			    	'dip_number'    => $query['dip_number'],
			    	'ed_estbl_name' => $query['ed_estbl_name'],
			    	'country_estbl' => $query['country_estbl'],
			    	'city_estbl'    => $query['city_estbl'],
			    	'year_grad'     => $query['year_grad'],
			    	'remarks'       => $query['remarks']
    	));

  }

  public static function getSelectInfo(){

  	$select = new stdClass;

	$select->countries      = Country::orderBy('name_en', 'asc')->lists('name_en', 'id');
  	$select->gender         = Gender::lists('name_en', 'id');
  	$select->eye_color      = EyeColor::lists('name_en', 'id');
  	$select->ranks 	        = Rank::lists('name_en', 'id');
  	$select->departments    = Department::lists('name_en', 'id');
  	$select->maritaltype    = Maritaltype::lists('name_en', 'id');
  	$select->hair_color     = HairColor::lists('name_en', 'id');
  	$select->status         = Status::lists('status_name', 'id');
  	$select->blood_groups   = BloodGroup::lists('name', 'id');
  	$select->ed_degree_lvls = EdDegreeLvl::lists('name', 'id');

  	return $select;
  }

}
