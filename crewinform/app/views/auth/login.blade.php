<?php 
/**
 * app/views/auth/login.blade.php
 */
?>
@extends('layout')
 
@section('main')
<div class="col-md-4 col-md-offset-4">

{{ Form::open(array('class' => 'form-signin')) }}

    @if (!$errors->isEmpty())
    <div class="alert alert-danger">
        @foreach ($errors->all() as $error)
        <p>{{ $error }}</p>
        @endforeach
    </div>
    @endif

    <h2 class="form-signin-heading">{{ $title }}</h2>

    {{ Form::text('username', null, array('class' => 'form-control', 'placeholder' => 'Login')) }}
    {{ Form::password('password', array('class' => 'form-control', 'placeholde' => 'Password')) }}

    <label class="checkbox">
        {{ Form::checkbox('remember-me', 1) }} Remember me
    </label>
    <a href="/register">Register</a>
    {{ Form::submit('Log in', array('class' => 'btn btn-lg btn-primary btn-block')) }}

{{ Form::close() }}
</div>
@stop