<?php 
/**
 * app/views/template/header.blade.php
 */
?>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>{{ $title }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">  
    <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW"> 
    @section('styles')
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

        <!-- Optional theme -->
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="{{ URL::asset('styles/datepicker.css') }}" type="text/css" media="screen" />
        <link rel="stylesheet" href="{{ URL::asset('styles/style.css') }}" type="text/css" media="screen" />
        <!-- Latest compiled and minified JavaScript -->
    @show
    @section('scripts')
        {{ HTML::script(URL::asset('scripts/jquery.min.js')) }} 
         <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script> 
         {{ HTML::script(URL::asset('js/bootstrap-datepicker.js')) }} 
         {{ HTML::script(URL::asset('js/bootstrap-filestyle.min.js')) }} 
    @show
</head>
<body>