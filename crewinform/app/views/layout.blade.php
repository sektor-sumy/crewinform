<?php 
/**
 * app/views/layout.blade.php
 */
?>
@include('template.header')
@include('template.nav')
  @yield('main')
@include('template.footer')