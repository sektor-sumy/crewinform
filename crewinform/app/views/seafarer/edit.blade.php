<?php 
/**
 * app/views/auth/homepage.blade.php
 */
?>
@extends('layout')

@section('main')
<div class="panel panel-info">
<div class="panel-heading">Edit Seafarer</div>
<div class="panel-body">
{{ Form::model($seafarer, array('method' => 'PUT', 'route' =>
 array('seafarer.update', $seafarer->id), 'files' => true)) }}
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#panel1">Personal data</a></li>
      <li><a data-toggle="tab" href="#panel2">Job preferences</a></li>
      <li><a data-toggle="tab" href="#panel3">Contact + Nok info</a></li>
      <li><a data-toggle="tab" href="#panel4">Biometrics</a></li>
      <li><a data-toggle="tab" href="#panel5">Educational history + T&T</a></li>
      <li><a data-toggle="tab" href="#panel6">Documents</a></li>
      <li><a data-toggle="tab" href="#panel7">Service Records</a></li>
      <li><a data-toggle="tab" href="#panel8">Bank account details</a></li>
      <li><a data-toggle="tab" href="#panel9">Notes & Refs</a></li>
    </ul>
      <div class="tab-content">
        <div id="panel1" class="tab-pane fade in active col-md-12">
          <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#personal_en">En</a></li>
            <li><a data-toggle="tab" href="#personal_ru">Ru</a></li>
          </ul>
          <div class="tab-content col-md-8">
          <div id="personal_en" class="tab-pane fade in active col-md-12">
            <h3>Personal data</h3>
            <div class="col-md-4">
            <div >
                {{ Form::label('lname', 'Surname') }}
                {{ Form::text('last_name', $seafarer->last_name, array('class' => 'form-control')) }}
              </div>
              <div >
                {{ Form::label('firstname', 'First name') }}
                {{ Form::text('first_name', $seafarer->first_name, array('class' => 'form-control')) }}
              </div>
              <div >
                {{ Form::label('fathers_name', 'Father\'s name') }}
                {{ Form::text('fathers_name', $seafarer->fathers_name, array('class' => 'form-control')) }}
              </div>
              <div >
                {{ Form::label('mothers_name', 'Mother\'s name') }}
                {{ Form::text('mothers_name', $seafarer->mothers_name, array('class' => 'form-control')) }}
              </div>
              <div >
                {{ Form::label('date_of_birth', 'Date of birth') }}
                <div class="date">
                  {{ Form::text('date_of_birth', $seafarer->date_of_birth, array('class' => 'datepicker form-control', 'data-date-format' => "yyyy-mm-dd")) }}
                  <span class="add-on"><i class="icon-th"></i></span>
                </div>
              </div>
              <div >
                {{ Form::label('country_of_birth', 'Country of birth') }}
                {{ Form::select('country_of_birth', array($seafarer->country_of_birth => $select->countries[$seafarer->country_of_birth]) + $select->countries, 'default', ['id' => 'city_of_birth', 'class' => 'form-control country']) }}
              </div>
              <div >
                 {{ Form::label('city_of_birth', 'City of birth') }}
                <div id="position_brith">
                   {{ Form::select('city_of_birth', array($seafarer->city_of_birth => $city[$seafarer->city_of_birth]) + $city, 'default', ['id' => 'city2', 'class' => 'form-control']) }}
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div >
                  {{ Form::label('citizenship', 'Citizenship') }}
                  {{ Form::select('citizenship', array($seafarer->citizenship => $select->countries[$seafarer->citizenship]) + $select->countries, 'default', ['id' => 'citizenship', 'class' => 'form-control']) }}
              </div>
              <div >
                  {{ Form::label('Addrcountry', 'Country of residence') }}
                  {{ Form::select('addrcountry', array($seafarer->Addrcountry => $select->countries[$seafarer->Addrcountry]) + $select->countries, 'default', ['id' => 'addrcity', 'class' => 'form-control country']) }}
              </div>
              <div >
                   {{ Form::label('Addrcity', 'City of residence') }}
                <div id="position">
                   {{ Form::select('addrcity', array($seafarer->Addrcity => $city[$seafarer->Addrcity]) + $city, 'default', ['id' => 'city1', 'class' => 'form-control']) }}
                </div>
              </div>
               <div >
                   {{ Form::label('address', 'Home address') }}
                   {{ Form::text('address', $seafarer->address, array('class' => 'form-control')) }}
                </div>  
                <div >
                  {{ Form::label('postalcode', 'Postal code') }}
                  {{ Form::text('postalcode', $seafarer->postalcode, array('class' => 'form-control')) }}
                </div>
                <div >
                   {{ Form::label('nearest_airport', 'Nearest airport') }}
                   {{ Form::text('nearest_airport', $seafarer->nearest_airport, array('class' => 'form-control')) }}
                </div>
            </div>
            <div class="col-md-4">
                <div >
                  {{ Form::label('maritalstatus', 'Marital status') }}
                  {{ Form::select('maritalstatus', array($seafarer->Maritalstatus => $select->maritaltype[$seafarer->Maritalstatus]) + $select->maritaltype, 'default',['id' => 'maritaltype', 'class' => 'form-control'])  }}
               </div>
               <!--
                <div >
                        {{ Form::label('readiness', 'readiness') }}
                        <div class="input-append date">
                          {{ Form::text('readiness', $seafarer->readiness, array('class' => 'datepicker', 'data-date-format' => "yyyy-mm-dd")) }}
                          <span class="add-on"><i class="icon-th"></i></span>
                        </div>
                </div>
                <div >
                   {{ Form::label('planned_vessel', 'Planned vessel') }}
                   {{ Form::text('planned_vessel', $seafarer->planned_vessel, array('class' => 'form-control')) }}
                </div>
                -->
            </div>
            </div>
            <div id="personal_ru" class="tab-pane fade">
              <h3>Personal data(RU)</h3>
              <div class="col-md-6">
                <div >
                  {{ Form::label('first_name_ru', 'First name (RU)') }}
                  {{ Form::text('first_name_ru', $seafarer->first_name_ru, array('class' => 'form-control')) }}
                </div>
                <div >
                  {{ Form::label('last_name_ru', 'Last Name (RU)') }}
                  {{ Form::text('last_name_ru', $seafarer->last_name_ru, array('class' => 'form-control')) }}
                </div>
                <div >
                  {{ Form::label('middle_name_ru', 'Middle Name (RU)') }}
                  {{ Form::text('middle_name_ru', $seafarer->middle_name_ru, array('class' => 'form-control')) }}
                </div>
              </div> 
        </div>
        </div>
          <div class="col-md-4">
            <div >
              <div id="list_img"><img src='{{ $seafarer->avatar }}' class='img-thumbnail' /></div>
              {{ Form::label('img', 'Avatar') }} 
              <span class="btn btn-file">
                  <input type="file" name="img" id="photo_upload" class="filestyle" data-input="false" />
              </span>
            </div>
          </div> 
        </div>
        <div id="panel2" class="tab-pane fade col-md-8 col-md-offset-2">
          <h3>Job preferences</h3>  
        </div> 
        <div id="panel3" class="tab-pane fade col-md-8 col-md-offset-2">
          <h3>Contact + Nok info</h3>  
        </div>    
        <div id="panel4" class="tab-pane fade col-md-8 col-md-offset-2">
          <h3>Biometrics</h3> 
          <div class="col-md-4">
          <div >
            {{ Form::label('Gender', 'Gender') }}
            {{ Form::select('gender', array($seafarer->Gender => $select->gender[$seafarer->Gender]) + $select->gender, 'default',['id' => 'gender', 'class' => 'form-control'])  }}
          </div>
          <div >
            {{ Form::label('blood_group', 'Blood group') }}
            {{ Form::select('blood_group', array($seafarer->blood_group => $select->blood_groups[$seafarer->blood_group]) + $select->blood_groups, 'default',['id' => 'blood_group', 'class' => 'form-control'])  }}
          </div>
          <div >
            {{ Form::label('Eyescolour', 'Eyes colour') }}
            {{ Form::select('eyescolour', array($seafarer->Eyescolour => $select->eye_color[$seafarer->Eyescolour]) + $select->eye_color, 'default',['id' => 'eye_color', 'class' => 'form-control'])  }}
          </div> 
           <div >
            {{ Form::label('Haircolour', 'Hair colour') }}
            {{ Form::select('haircolour', array($seafarer->Haircolour => $select->hair_color[$seafarer->Haircolour]) + $select->hair_color, 'default',['id' => 'hair_color', 'class' => 'form-control'])  }}
          </div>
          </div>
          <div class="col-md-4">
              <div >
                {{ Form::label('height_cm', 'Height, cm') }}
                {{ Form::text('height_cm', $seafarer->height_cm, array('class' => 'form-control')) }}
              </div>
              <div >
                {{ Form::label('weight_kg', 'Weight, cm') }}
                {{ Form::text('weight_kg', $seafarer->weight_kg, array('class' => 'form-control')) }}
              </div>  
               <div >
                {{ Form::label('chest', 'Chest, cm') }}
                {{ Form::text('chest', $seafarer->chest, array('class' => 'form-control')) }}
              </div>   
               <div >
                {{ Form::label('waist', 'Waist, cm') }}
                {{ Form::text('waist', $seafarer->waist, array('class' => 'form-control')) }}
              </div>
              <div >
                {{ Form::label('hips', 'Hips, cm') }}
                {{ Form::text('hips', $seafarer->hips, array('class' => 'form-control')) }}
              </div>
              <div >
                  {{ Form::label('boilersuit', 'Overall size (EU)') }}
                  {{ Form::text('boilersuit', $seafarer->boilersuit, array('class' => 'form-control')) }}
              </div>
            <div >
              {{ Form::label('boots', 'Boots size (EU)') }}
              {{ Form::text('boots', $seafarer->boots, array('class' => 'form-control')) }}
            </div> 
          </div>
          <div class="col-md-4">        
          <div >
            {{ Form::label('height_ft', 'Height, ft in') }}
            {{ Form::text('height_ft', $seafarer->height_ft, array('class' => 'form-control')) }}
          </div>   
          <div >
            {{ Form::label('weight_lb', 'Weight, lb') }}
            {{ Form::text('weight_lb', $seafarer->weight_lb, array('class' => 'form-control')) }}
          </div>   
         </div>
        </div>
        <div id="panel5" class="tab-pane fade col-md-8 col-md-offset-2">
          <h3>Educational Historу</h3>  
        </div> 
        <div id="panel6" class="tab-pane fade col-md-4 col-md-offset-4">
          <h3>Documents</h3>
            <div >
                  {{ Form::label('ed_degree_lvl', 'Educational Degree Level') }}
                  {{ Form::select('ed_degree_lvl', array($history->ed_degree_lvl => $select->ed_degree_lvls[$history->ed_degree_lvl]) + $select->ed_degree_lvls, 'default', ['id' => 'ed_degree_lvl', 'class' => 'form-control']) }}
            </div>
            <div >
                {{ Form::label('dip_number', 'Number of Diploma') }}
                {{ Form::text('dip_number', $history->dip_number, array('class' => 'form-control')) }}
            </div>
            <div >
                {{ Form::label('ed_estbl_name', 'Educational Establishment Name ') }}
                {{ Form::text('ed_estbl_name', $history->ed_estbl_name, array('class' => 'form-control')) }}
            </div>
            <div >
              {{ Form::label('country_estbl', 'Country of Establishment') }}
              {{ Form::select('country_estbl', array($history->country_estbl => $select->countries[$history->country_estbl]) + $select->countries, 'default', ['id' => 'city_estbl', 'class' => 'form-control country']) }}
            </div>
            <div >
                {{ Form::label('city_estbl', 'City of Establishmen') }}
                 <div id="position_birth">
                  {{ Form::select('city_estbl', array($history->city_estbl => $city[$history->city_estbl]) + $city, 'default', ['id' => 'city3', 'class' => 'form-control']) }}
                </div>
            </div>
            <div >
                {{ Form::label('year_grad', 'Year of graduation') }}
                 <div class="input-append date">
                      {{ Form::text('year_grad', $history->year_grad, array('class' => 'datepicker', 'data-date-format' => "yyyy-mm-dd")) }}
                      <span class="add-on"><i class="icon-th"></i></span>
                </div>
            </div>
            <div >
                {{ Form::label('remarks', 'Remarks') }}
                {{ Form::text('remarks', $history->remarks, array('class' => 'form-control')) }}
            </div>
        </div>
        <div id="panel7" class="tab-pane fade col-md-8 col-md-offset-2">
          <h3>Service Records</h3>  
        </div> 
        <div id="panel8" class="tab-pane fade col-md-8 col-md-offset-2">
          <h3>Bank account details</h3>  
        </div> 
        <div id="panel9" class="tab-pane fade col-md-4 col-md-offset-4">
          <h3>Notes & Refs</h3> 
        </div> 
      </div>
      <div class="container-fluid ">
        <div class="row col-md-4 col-md-offset-4">
              {{ Form::submit('Submit', array('class' => 'btn btn-primary')) }}
        </div>
      </div>

{{ Form::close() }}

@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
</div>
@endif
<script type="text/javascript">
$(":file").filestyle({input: false});
function showFile(e) {
    var files = e.target.files;
    var name = e.target.name;
    for (var i = 0, f; f = files[i]; i++) {
      if (!f.type.match('image.*')) continue;
      var fr = new FileReader();
      fr.onload = (function(theFile) {
        return function(e) {
          var li = document.createElement('span');
          li.innerHTML = "<img src='" + e.target.result + "' class='img-thumbnail' />";
          $('#list_' + name).empty();
          document.getElementById('list_' + name).insertBefore(li, null);
        };
      })(f);
 
      fr.readAsDataURL(f);
    }
  }
 
  document.getElementById('photo_upload').addEventListener('change', showFile, false);

$('.datepicker').datepicker({
    startDate: '-3d',
})
$( ".country" ).change(function() 
  {
    var test = this.id;
    $.getJSON("/seafarer/"+ $(this).val() +"/city", function(jsonData){
        select = '<select name="'+test+'" class="form-control input-sm " required >';
          $.each(jsonData, function(i,data)
          {
               select +='<option value="'+data.id+'">'+data.name_en+'</option>';
           });
        select += '</select>';
        $("#position_"+test).html(select);
    });
  });
</script>

@stop