<?php 
/**
 * app/views/auth/homepage.blade.php
 */
?>
@extends('layout')

@section('main')

<div class="panel panel-info">
<div class="panel-heading">Create Seafarer</div>
<div class="panel-body">
{{ Form::open(array('route' => 'seafarer.store', 'files' => true)) }}
 @if (!$errors->isEmpty())
    <div class="alert alert-danger">
        @foreach ($errors->all() as $error)
        <p>{{ $error }}</p>
        @endforeach
    </div>
    @endif
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#panel1">Personal data</a></li>
      <li><a data-toggle="tab" href="#panel2">Job preferences</a></li>
      <li><a data-toggle="tab" href="#panel3">Contact + Nok info</a></li>
      <li><a data-toggle="tab" href="#panel4">Biometrics</a></li>
      <li><a data-toggle="tab" href="#panel5">Educational history + T&T</a></li>
      <li><a data-toggle="tab" href="#panel6">Documents</a></li>
      <li><a data-toggle="tab" href="#panel7">Service Records</a></li>
      <li><a data-toggle="tab" href="#panel8">Bank account details</a></li>
      <li><a data-toggle="tab" href="#panel9">Notes & Refs</a></li>
    </ul>
      <div class="tab-content">
        <div id="panel1" class="tab-pane fade in active col-md-12">
          <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#personal_en">EN</a></li>
            <li><a data-toggle="tab" href="#personal_ru">RU</a></li>
          </ul>
          <div class="tab-content col-md-8">
          <div id="personal_en" class="tab-pane fade in active">
              <h3>Personal data</h3>
              <div class="col-md-4">
                  <div >
                    {{ Form::label('lname', 'Surname') }}
                    {{ Form::text('last_name', '', array('class' => 'form-control')) }}
                  </div> 
                  <div >
                    {{ Form::label('first_name', 'First name') }}
                    {{ Form::text('first_name', '', array('class' => 'form-control')) }}
                  </div>
                  <div >
                    {{ Form::label('fathers_name', 'Father\'s name') }}
                    {{ Form::text('fathers_name', '', array('class' => 'form-control')) }}
                  </div>
                  <div >
                    {{ Form::label('mothers_name', 'Mother\'s name') }}
                    {{ Form::text('mothers_name', '', array('class' => 'form-control')) }}
                  </div>
                  <div >
                    {{ Form::label('date_of_birth', 'Date of birth') }}
                    <div class="date">
                      {{ Form::text('date_of_birth', '', array('class' => 'datepicker form-control', 'data-date-format' => "yyyy-mm-dd")) }}
                      <span class="add-on"><i class="icon-th"></i></span>
                    </div>
                  </div>
                   <div >
                      {{ Form::label('country_of_birth', 'Country of birth') }}
                      {{ Form::select('country_of_birth', array('default' => 'Please Select') + $seafarer->countries, null, ['id' => 'city_of_birth', 'class' => 'form-control country']) }}
                    </div>
                    <div >
                        {{ Form::label('city_of_birth', 'City of birth') }}
                         <div id="position_city_of_birth">
                          {{ Form::select('city_of_birth', array('default' => 'Please Select Country'), null, ['id' => 'city1', 'class' => 'form-control']) }}
                        </div>
                    </div>
                   
              </div>
              <div class="col-md-4"> 
                  <div >
                      {{ Form::label('citizenship', 'Citizenship') }}
                      {{ Form::select('citizenship', array('default' => 'Please Select') + $seafarer->countries, null, ['id' => 'ccitizenship', 'class' => 'form-control']) }}
                    </div>
                    <div >
                      {{ Form::label('addrcountry', 'Country of residence') }}
                      {{ Form::select('addrcountry', array('default' => 'Please Select') + $seafarer->countries, null, ['id' => 'addrcity', 'class' => 'form-control country']) }}
                    </div>
                    <div >
                       {{ Form::label('addrcity', 'City of residence') }}
                     <div id="position_addrcity">
                          {{ Form::select('addrcity', array('default' => 'Please Select Country'), null, ['id' => 'city1', 'class' => 'form-control']) }}
                        </div>
                    </div>
                    <div >
                       {{ Form::label('Address', 'Home address') }}
                       {{ Form::text('address', '', array('class' => 'form-control')) }}
                    </div>
                     <div >
                      {{ Form::label('postalcode', 'Postal code') }}
                      {{ Form::text('postalcode', '', array('class' => 'form-control')) }}
                    </div>
                     <div >
                       {{ Form::label('nearest_airport', 'Nearest airport') }}
                       {{ Form::text('nearest_airport', '', array('class' => 'form-control')) }}
                    </div>
                </div>
                <div class="col-md-4"> 
                    <div >
                      {{ Form::label('maritalstatus', 'Marital status') }}
                      {{ Form::select('maritalstatus', array('default' => 'Please Select') + $seafarer->maritaltype, null,['id' => 'maritaltype', 'class' => 'form-control'])  }}
                   </div>  
                  
                    <!--<div >
                      {{ Form::label('status', 'Status') }}
                      {{ Form::select('status', array('default' => 'Please Select') + $seafarer->status, 'default',['id' => 'status', 'class' => 'form-control'])  }}
                   </div> 
                    <div >
                        {{ Form::label('readiness', 'Readiness') }}
                        <div class="input-append date">
                          {{ Form::text('readiness', '', array('class' => 'datepicker', 'data-date-format' => "yyyy-mm-dd")) }}
                          <span class="add-on"><i class="icon-th"></i></span>
                        </div>
                    </div> 
                    <div >
                       {{ Form::label('planned_vessel', 'Planned vessel') }}
                       {{ Form::text('planned_vessel', '', array('class' => 'form-control')) }}
                    </div>--> 
                </div>
             </div>
             <div id="personal_ru" class="tab-pane fade">
              <h3>Personal data(RU)</h3>
              <div class="col-md-6">
                <div >
                  {{ Form::label('first_name_ru', 'First name (RU)') }}
                  {{ Form::text('first_name_ru', '', array('class' => 'form-control')) }}
                </div>
                <div >
                  {{ Form::label('last_name_ru', 'Last Name (RU)') }}
                  {{ Form::text('last_name_ru', '', array('class' => 'form-control')) }}
                </div>
                <div >
                  {{ Form::label('middle_name_ru', 'Middle Name (RU)') }}
                  {{ Form::text('middle_name_ru', '', array('class' => 'form-control')) }}
                </div>
              </div> 
            </div>
            </div>
            <div class="col-md-4">
               <div >
                <div id="list_img"></div>
                {{ Form::label('img', 'Avatar') }} 
                <span class="btn btn-file">
                    <input type="file" name="img" id="photo_upload" class="filestyle" data-input="false" />
                </span>
              </div>
            </div>
        </div>
        <div id="panel2" class="tab-pane fade col-md-8 col-md-offset-2">
          <h3>Job preferences</h3>  
        </div> 
        <div id="panel3" class="tab-pane fade col-md-8 col-md-offset-2">
          <h3>Contact + Nok info</h3>  
        </div>   
        <div id="panel4" class="tab-pane fade col-md-8 col-md-offset-2">
          <h3>Biometrics</h3> 
          <div class="col-md-4">
              <div >
                {{ Form::label('Gender', 'Gender') }}
                {{ Form::select('gender', array('2' => $seafarer->gender[2]) + $seafarer->gender, 'default',['id' => 'gender', 'class' => 'form-control'])  }}
              </div> 
              <div >
                  {{ Form::label('blood_group', 'Blood group') }}
                  {{ Form::select('blood_group', array('default' => 'Please Select') + $seafarer->blood_groups, null, ['id' => 'blood_group', 'class' => 'form-control']) }}
              </div>
               <div >
                {{ Form::label('Eyescolour', 'Eyes colour') }}
                {{ Form::select('eyescolour', array('default' => 'Please Select') + $seafarer->eye_color, 'default',['id' => 'eyescolour', 'class' => 'form-control'])  }}
              </div> 
               <div >
                {{ Form::label('Haircolour', 'Hair colour') }}
                {{ Form::select('haircolour', array('default' => 'Please Select') + $seafarer->hair_color, 'default',['id' => 'hair_color', 'class' => 'form-control'])  }}
              </div>
            </div>
            <div class="col-md-4">
                <div >
                    {{ Form::label('height_cm', 'Height, cm') }}
                    {{ Form::text('height_cm', '', array('class' => 'form-control')) }}
               </div>  
               <div >
                {{ Form::label('weight_kg', 'Weight, cm') }}
                {{ Form::text('weight_kg', '', array('class' => 'form-control')) }}
              </div>  
                <div >
                    {{ Form::label('Chest', 'Chest, cm') }}
                    {{ Form::text('chest', '', array('class' => 'form-control')) }}
              </div>  
              <div >
                    {{ Form::label('waist', 'Waist, cm') }}
                    {{ Form::text('waist', '', array('class' => 'form-control')) }}
              </div>
              <div >
                    {{ Form::label('hips', 'Hips, cm') }}
                    {{ Form::text('hips', '', array('class' => 'form-control')) }}
              </div>   
               <div >
                    {{ Form::label('boilersuit', 'Overall, size (EU)') }}
                    {{ Form::text('boilersuit', '', array('class' => 'form-control')) }}
              </div>  
              <div >
                    {{ Form::label('Boots', 'Boots, size (EU)') }}
                    {{ Form::text('boots', '', array('class' => 'form-control')) }}
              </div>   
            </div>
            <div class="col-md-4"> 
              <div>
                {{ Form::label('height_ft', 'Height, ft in') }}
                {{ Form::text('height_ft', '', array('class' => 'form-control')) }}
              </div>    
              <div>
                {{ Form::label('weight_lb', 'Weight, lb') }}
                {{ Form::text('weight_lb', '', array('class' => 'form-control')) }}
              </div>
            </div> 
        </div>
        <div id="panel5" class="tab-pane fade col-md-8 col-md-offset-2">
          <h3>Educational Historу</h3>  
        </div> 
        <div id="panel6" class="tab-pane fade col-md-4 col-md-offset-4">
          <h3>Documents</h3>
            <div >
                  {{ Form::label('ed_degree_lvl', 'Educational Degree Level') }}
                  {{ Form::select('ed_degree_lvl', array('default' => 'Please Select') + $seafarer->ed_degree_lvls, null, ['id' => 'ed_degree_lvl', 'class' => 'form-control']) }}
            </div>
            <div >
                {{ Form::label('dip_number', 'Number of Diploma') }}
                {{ Form::text('dip_number', '', array('class' => 'form-control')) }}
            </div>
            <div >
                {{ Form::label('ed_estbl_name', 'Educational Establishment Name ') }}
                {{ Form::text('ed_estbl_name', '', array('class' => 'form-control')) }}
            </div>
            <div >
              {{ Form::label('country_estbl', 'Country of Establishment') }}
              {{ Form::select('country_estbl', array('default' => 'Please Select') + $seafarer->countries, null, ['id' => 'city_estbl', 'class' => 'form-control country']) }}
            </div>
            <div >
                {{ Form::label('city_estbl', 'City of Establishmen') }}
                 <div id="position_city_estbl">
                  {{ Form::select('city_estbl', array('default' => 'Please Select Country'), null, ['id' => 'city3', 'class' => 'form-control']) }}
                </div>
            </div>
            <div >
                {{ Form::label('year_grad', 'Year of graduation') }}
                 <div class="input-append date">
                      {{ Form::text('year_grad', '', array('class' => 'datepicker', 'data-date-format' => "yyyy-mm-dd")) }}
                      <span class="add-on"><i class="icon-th"></i></span>
                </div>
            </div>
            <div >
                {{ Form::label('remarks', 'Remarks') }}
                {{ Form::text('remarks', '', array('class' => 'form-control')) }}
            </div>

        </div>
        <div id="panel7" class="tab-pane fade col-md-8 col-md-offset-2">
          <h3>Service Records</h3>  
        </div> 
        <div id="panel8" class="tab-pane fade col-md-8 col-md-offset-2">
          <h3>Bank account details</h3>  
        </div> 
        <div id="panel9" class="tab-pane fade col-md-4 col-md-offset-4">
          <h3>Notes & Refs</h3> 
         
        </div>
      <div class="container-fluid ">
        <div class="row col-md-4 col-md-offset-4">
              {{ Form::submit('Submit', array('class' => 'btn btn-primary')) }}
        </div>
      </div>
</div>
{{ Form::close() }}

@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
@endif
<script type="text/javascript">
$(":file").filestyle({input: false});
function showFile(e) {
    var files = e.target.files;
    var name = e.target.name;
    for (var i = 0, f; f = files[i]; i++) {
      if (!f.type.match('image.*')) continue;
      var fr = new FileReader();
      fr.onload = (function(theFile) {
        return function(e) {
          var li = document.createElement('span');
          li.innerHTML = "<img src='" + e.target.result + "' class='img-thumbnail' />";
         $('#list_' + name).empty();
          document.getElementById('list_' + name).insertBefore(li, null);
        };
      })(f);
 
      fr.readAsDataURL(f);
    }
  }
 
  document.getElementById('photo_upload').addEventListener('change', showFile, false);


$('.datepicker').datepicker({
    startDate: '-3d',
})
$( ".country" ).change(function() 
  {
    var test = this.id;
    $.getJSON("/seafarer/"+ $(this).val() +"/city", function(jsonData){
        select = '<select name="'+test+'" class="form-control input-sm " required >';
          $.each(jsonData, function(i,data)
          {
               select +='<option value="'+data.id+'">'+data.name_en+'</option>';
           });
        select += '</select>';
        $("#position_"+test).html(select);
    });
  });
</script>
@stop