<?php 
/**
 * app/views/auth/homepage.blade.php
 */
?>
@extends('layout')

@section('main')
<h1>Seafarer of {{ $seaf->first_name }}</h1>
<div class="container-fluid ">
<p>{{ link_to_route('seafarer.create', 'Add new seafarer') }}</p>
   <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#panel1">Personal data</a></li>
      <li><a data-toggle="tab" href="#panel2">Biometrics</a></li>
      <li><a data-toggle="tab" href="#panel3">Additional</a></li>
    </ul>
      <div class="tab-content">
        <div id="panel1" class="tab-pane fade in active col-md-6 col-md-offset-2">
          <h3>Personal data</h3>
      <table class="table table-striped table-bordered">
          <tbody>
            <tr>
               <th>Last name</th><td>{{ $seaf->last_name }}</td>
            </tr><tr>
               <th>First name</th><td>{{ $seaf->first_name }}</td>
            </tr><tr>
               <th>Fathers name</th><td>{{ $seaf->fathers_name }}</td>
            </tr><tr>
               <th>Mothers name</th><td>{{ $seaf->mothers_name }}</td>
            </tr><tr>
               <th>Dateofbirth</th><td>{{ $seaf->date_of_birth }}</td>
            </tr><tr>
               <th>Placeofbirth</th><td>{{ $seaf->place_of_birth }}</td>
            </tr><tr>
               <th>Citizenship</th><td>{{ $seaf->citizenship }}</td>
            </tr><tr>
               <th>Citizenshiptype</th><td>{{ $seaf->citizenship_type }}</td>
            </tr><tr>
               <th>Maritalstatus</th><td>{{ $seaf->marital_name }}</td>
            </tr><tr>
               <th>Phone</th><td>{{ $seaf->phone }}</td>
            </tr><tr>
               <th>Mobile</th><td>{{ $seaf->mobile }}</td>
            </tr><tr>
               <th>Skype</th><td>{{ $seaf->skype }}</td>
            </tr><tr>
               <th>Country of residence</th><td>{{ $seaf->country }}</td>
            </tr><tr>
               <th>City of residence</th><td>{{ $seaf->city }}</td>
            </tr><tr>
               <th>Postalcode</th><td>{{ $seaf->postalcode }}</td>
            </tr><tr>
               <th>Rank</th><td>{{ $seaf->rank }}</td>
            </tr><tr>
               <th>Rank Type</th><td>{{ $seaf->ranktype }}</td>
            </tr><tr>
               <th>Email</th><td>{{ $seaf->email }}</td>
            </tr><tr>
               <th>Address</th><td>{{ $seaf->address }}</td>
            </tr><tr>
               <th>Department</th><td>{{ $seaf->department }}</td>
            </tr>         
          </tbody>
      </table>
    </div>
      <div id="panel2" class="tab-pane fade col-md-6 col-md-offset-2">
        <h3>Biometrics</h3> 
        <table class="table table-striped table-bordered">
          <tbody>
            <tr>
               <th>Gender</th><td>{{ $seaf->name_en }}</td>
            </tr><tr>
               <th>Height, cm</th><td>{{ $seaf->height }}</td>
            </tr><tr>
               <th>Weight, lb</th><td>{{ $seaf->weight}}</td>
            </tr><tr>
               <th>Chest</th><td>{{ $seaf->chest }}</td>
            </tr><tr>
               <th>Boilersuit</th><td>{{ $seaf->boilersuit }}</td>
            </tr><tr>
               <th>Boots</th><td>{{ $seaf->boots }}</td>
            </tr><tr>
               <th>Sweater</th><td>{{ $seaf->sweater }}</td>
            </tr><tr>
               <th>Trousers</th><td>{{ $seaf->trousers }}</td>
            </tr><tr>
               <th>Pilotshirt</th><td>{{ $seaf->pilotshirt }}</td>
            </tr><tr>
               <th>Eyes colour</th><td>{{ $seaf->eye_name }}</td>
            </tr><tr>
               <th>Hair colour</th><td>{{ $seaf->hair_name }}</td>
            </tr>         
          </tbody>
        </table>
      </div>
       <div id="panel3" class="tab-pane fade col-md-6 col-md-offset-2">
       <h3>Additional</h3>
        <table class="table table-striped table-bordered">
          <tbody>
            <tr>
              <td>Author</td><td>Text</td>
            </tr>
            @foreach($comments as $comment)
            <tr>
              <td>{{$comment->username}}</td><td>{{$comment->comment}}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      {{ Form::open(array('route' => 'seafarer.comment', 'files' => true)) }}
      <div class="form-group">
        {{ Form::label('comment', 'Add comment') }}
        {{ Form::textarea('comment', null, ['size' => '30x5', 'class' => 'form-control']) }}
        {{ Form::hidden('seafarer_id', $seaf->id) }}
      </div>
        {{ Form::submit('Submit', array('class' => 'btn btn-primary')) }}
        {{ Form::close() }}
      </div>
    </div>
</div>

@stop