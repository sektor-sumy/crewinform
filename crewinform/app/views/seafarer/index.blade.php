<?php 
/**
 * app/views/auth/homepage.blade.php
 */
?>
@extends('layout')

@section('main')
<div class="modal fade bs-example-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">DELETE</h4>
      </div>
      <div class="modal-body" id="modaltext">
      </div>
      <div class="modal-footer">
        <button id="butOk" type="submit" name="delete" value="" class="btn btn-danger" data-token="{{ csrf_token() }}">OK</button>
        <button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
function newVal(t){
var res = $(t).attr('value').split(','); 
var text = '<p>Delete ' +res[1]+' '+res[0]+ '. Are you sure?</p>';
$('#modaltext').html(text);
$('#butOk').val(res[res.length-1]);
   return false;
}
$('#butOk').click(function(){
    var url = this.baseURI +'/'+this.value+'/destroy';
    var token = this.dataset.token;
    $.ajax({
        type: "DELETE",
        url: url,
        data: {_method: 'delete', _token :token},
        dataType: "json",
         success: function(result) {
            console.log('You have deleted me.');
        },
        error: function() {
           console.log('You have not deleted me.');
        }
    });
    location.reload();
});
</script>
<h1>All Seafarer</h1>

<p>{{ link_to_route('seafarer.create', 'Add new seafarer') }}</p>
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
        <th>Last name</th>
        <th>First name</th>
        <th>Date of birth</th>
        <th>Email</th>
        <th>Address</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($seafarer as $seaf)
                <tr>
                    <td>{{ $seaf->last_name }}</td>
          <td>{{ $seaf->first_name }}</td>
          <td>{{ $seaf->date_of_birth }}</td>
          <td>{{ $seaf->email }}</td>
          <td>{{ $seaf->address }}</td>
                    <td>{{ link_to_route('seafarer.show', 'Show',
 array($seaf->id), array('class' => 'btn btn-info')) }}</td>
                    <td>{{ link_to_route('seafarer.edit', 'Edit',
 array($seaf->id), array('class' => 'btn btn-info')) }}</td>
                    <td><a data-toggle="modal" value="{{ $seaf->first_name }},{{ $seaf->last_name }},{{ $seaf->id }}" data-target=".bs-example-modal" onclick="newVal(this)" href="#"><span class="glyphicon glyphicon-remove btn-danger"></span></a>
          
                    </td>
                </tr>
            @endforeach
              
        </tbody>
      
    </table>

<script type="text/javascript">
    $('document').ready(function(){
    $('#modal').modal();
});
</script>
@stop