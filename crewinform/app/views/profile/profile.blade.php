<?php 
/**
 * app/views/auth/homepage.blade.php
 */
?>
@extends('layout')

@section('main')


<div class="container">
@if (!$errors->isEmpty())
    <div class="alert alert-danger">
        @foreach ($errors->all() as $error)
        <p>{{ $error }}</p>
        @endforeach
    </div>
    @endif

   <div class="panel panel-default">
  <!-- Default panel contents -->
      <div class="panel-heading">Profile {{ Sentry::getUser()->username}}</div>
      <!-- Table -->
      <table class="table">
      <tr>
          <th>name</th>
          <th>value</th>
      </tr>
      <tr>
          <td>Login</td>
          <td>{{ Sentry::getUser()->username}}</td>
      </tr>
      <tr>
          <td>First name</td>
          <td>{{ Sentry::getUser()->first_name}}</td>
      </tr>
      <tr>
          <td>Last name</td>
          <td>{{ Sentry::getUser()->last_name}}</td>
      </tr>
      <tr>
          <td>Email</td>
          <td>{{ Sentry::getUser()->email}}</td>
      </tr>
      <tr>
          <td>Created</td>
          <td>{{ Sentry::getUser()->created_at}}</td>
      </tr>
      </table>
 </div>
 <a href="/profile/{{ Sentry::getUser()->id}}/edit">Edit</a>


</div>
@stop